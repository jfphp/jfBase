# jf/base

Repositorios con clases y traits de utilidad para realizar composición y/o herencia.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/base` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/base
```

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone https://www.gitlab.com/jfphp/jfBase.git
cd jfBase
composer install
```

## Archivos disponibles

### Clases

| Nombre                                                                   | Descripción                                                                                                                                                                                                                   |
|:-------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [jf\Base\AAssign](src/AAssign.php)                                       | Clase que aporta la misma funcionalidad que `jf\Base\ASerializable` pero con un método `toArray()` más potente y que además implementa `jf\Base\IAssign` para facilitar la asignación de propiedades.                         |
| [jf\Base\AAssignExtras](src/AAssignExtras.php)                           | Clase que aporta la misma funcionalidad que `jf\Base\AAsign` permitiendo adicionalmente almacenar aquellos valores que se trataron de asignar pero que no corresponden con propiedades de la clase.                           |
| [jf\Base\ABase](src/ABase.php)                                           | Clase que puede ser usada como base para algunos proyectos que implementa `Stringable` y aporta un método `new` así algunos métodos para obtener el nombre de la clase en diferentes formatos así como su espacio de nombres. |
| [jf\Base\ArrayObject](src/ArrayObject.php)                               | Clase usada donde es requerido un objeto de tipo `\ArrayObject` pero que implemente `jf\Base\IAssign` e `jf\Base\IToArray`.                                                                                                   |
| [jf\Base\Exception](src/Exception.php)                                   | Excepciones del paquete `jf/base`.                                                                                                                                                                                            |
| [jf\Base\File\File](src/File/File.php)                                   | Lee/escribe contenido desde/a un archivo.                                                                                                                                                                                     |
| [jf\Base\File\Json](src/File/Json.php)                                   | Clase que sirve de base para aquellos objetos que hacen uso intenso de archivos JSON.                                                                                                                                         |
| [jf\Base\File\Yaml](src/File/Yaml.php)                                   | Clase que sirve de base para aquellos objetos que hacen uso intenso de archivos YAML.                                                                                                                                         |
| [jf\Base\Serializable\ASerializable](src/Serializable/ASerializable.php) | Clase base para aquellos objetos que necesitan serialización.                                                                                                                                                                 |
| [jf\Base\String\Tpl](src/String/Tpl.php)                                 | Renderiza plantillas de textos sencillas.                                                                                                                                                                                     |

### Interfaces

| Nombre                                         | Descripción                                                                                                             |
|:-----------------------------------------------|:------------------------------------------------------------------------------------------------------------------------|
| [jf\Base\IAssign](src/IAssign.php)             | Interfaz para detectar el uso de los métodos `assign` y `fromArray`.                                                    |
| [jf\Base\IId](src/IId.php)                     | Interfaz para los elementos que requieren un identificador.                                                             |
| [jf\Base\IParent](src/IParent.php)             | Gestiona el acceso a una instancia padre.                                                                               |
| [jf\Base\ISingleton](src/ISingleton.php)       | Interfaz para los singletons.                                                                                           |
| [jf\Base\IStringable](src/IStringable.php)     | Interfaz para detectar aquellas clases que se pueden convertir a `string` pero que no aceptan la interfaz `Stringable`. |
| [jf\Base\IToArray](src/IToArray.php)           | Interfaz para detectar el uso del método `toArray` y poder serializar un objeto.                                        |
| [jf\Base\ITranslatable](src/ITranslatable.php) | Interfaz para marcar aquellos objetos que necesiten traducir los valores presentes en sus propiedades.                  |

### Traits

| Nombre                                                                           | Descripción                                                                                                                                              |
|:---------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------|
| [jf\Base\Array\TArray](src/Array/TArray.php)                                     | Trait que permite implementar las interfaces `ArrayAccess`, `Countable`, `Iterator` e `IteratorAggregate` en una clase que se quiera usar como un array. |
| [jf\Base\Array\TArrayAccess](src/Array/TArrayAccess.php)                         | Trait que permite implementar la interfaz `ArrayAccess`.                                                                                                 |
| [jf\Base\Array\TArrayCountable](src/Array/TArrayCountable.php)                   | Trait que permite implementar la interfaz `Countable`.                                                                                                   |
| [jf\Base\Array\TArrayIterator](src/Array/TArrayIterator.php)                     | Trait que permite implementar la interfaz `Iterator`.                                                                                                    |
| [jf\Base\Array\TArrayIteratorAggregate](src/Array/TArrayIteratorAggregate.php)   | Trait que permite implementar la interfaz `IteratorAggregate`.                                                                                           |
| [jf\Base\File\TFile](src/File/TFile.php)                                         | Agrega funcionalidad básica a clases que requieren leer/escribir archivos.                                                                               |
| [jf\Base\File\TFilename](src/File/TFilename.php)                                 | Trait para manipular el nombre de un archivo.                                                                                                            |
| [jf\Base\File\TJson](src/File/TJson.php)                                         | Trait para gestionar archivos JSON.                                                                                                                      |
| [jf\Base\File\TPathInfo](src/File/TPathInfo.php)                                 | Trait para encapsular las llamadas a `pathinfo()`.                                                                                                       |
| [jf\Base\File\TReadWrite](src/File/TReadWrite.php)                               | Trait para gestionar la lectura y escritura de archivos.                                                                                                 |
| [jf\Base\File\TYaml](src/File/TYaml.php)                                         | Trait para gestionar archivos YAML.                                                                                                                      |
| [jf\Base\Serializable\TAssign](src/Serializable/TAssign.php)                     | Implementa la interfaz `jf\Base\IAssign` de manera simple asignando solamente las propiedades.                                                           |
| [jf\Base\Serializable\TJson](src/Serializable/TJson.php)                         | Trait para serializar datos en formato JSON.                                                                                                             |
| [jf\Base\Serializable\TJsonSerializable](src/Serializable/TJsonSerializable.php) | Trait que permite implementar la interfaz `JsonSerializable`.                                                                                            |
| [jf\Base\Serializable\TSerializable](src/Serializable/TSerializable.php)         | Trait que permite implementar de manera muy simple y rápida la serialización de objetos en arrays así como inicializar el objeto desde un array.         |
| [jf\Base\Serializable\TToArray](src/Serializable/TToArray.php)                   | Serializa el contenido de la instancia iterando sobre todas las propiedades y devolviendo los valores escalares, nulos y arrays.                         |
| [jf\Base\Serializable\TYaml](src/Serializable/TYaml.php)                         | Trait para gestionar datos en formato YAML.                                                                                                              |
| [jf\Base\String\TPlaceholders](src/String/TPlaceholders.php)                     | Trait para gestionar valores de reemplazo y renderizar textos.                                                                                           |
| [jf\Base\String\TString](src/String/TString.php)                                 | Trait para agrupar todos los traits que gestionan las cadenas de texto como arrays.                                                                      |
| [jf\Base\String\TStringAccess](src/String/TStringAccess.php)                     | Trait que permite implementar la interfaz `ArrayAccess` con una cadena de texto.                                                                         |
| [jf\Base\String\TStringCountable](src/String/TStringCountable.php)               | Trait que permite implementar la interfaz `Countable` con una cadena de texto.                                                                           |
| [jf\Base\String\TStringIterator](src/String/TStringIterator.php)                 | Trait que permite implementar la interfaz `Iterator` en una cadena de texto.                                                                             |
| [jf\Base\String\TStringable](src/String/TStringable.php)                         | Trait simple para implementar la interfaz `Stringable` devolviendo como texto el nombre dela clase.                                                      |
| [jf\Base\TAssign](src/TAssign.php)                                               | Permite asignar propiedades a una instancia a partir de un array.                                                                                        |
| [jf\Base\TAssignExtras](src/TAssignExtras.php)                                   | Permite asignar propiedades a una instancia a partir de un array y almacenar aquellas que no pudieron asignarse.                                         |
| [jf\Base\TClass](src/TClass.php)                                                 | Aporta algunos métodos y propiedades útiles para obtener el nombre de la clase y su espacio de nombres.                                                  |
| [jf\Base\TContent](src/TContent.php)                                             | Trait para gestionar el contenido de una instancia.                                                                                                      |
| [jf\Base\TDasherize](src/TDasherize.php)                                         | Aporta métodos para obtener el nombre de la clase y/o su espacio de nombre en formato `kebab-case`.                                                      |
| [jf\Base\TId](src/TId.php)                                                       | Permite implementar la interfaz `jf\Base\IId`.                                                                                                           |
| [jf\Base\TOffset](src/TOffset.php)                                               | Trait para calcular el offset en las clases que implementan `ArrayAccess` y aceptan el formato `[from:to]` para acceder a los valores.                   |
| [jf\Base\TParent](src/TParent.php)                                               | Gestiona el acceso a una instancia padre.                                                                                                                |
| [jf\Base\TPascalize](src/TPascalize.php)                                         | Aporta un método para convertir un texto a formato `PascalCase`.                                                                                         |
| [jf\Base\TSingleton](src/TSingleton.php)                                         | Contiene algunos métodos y propiedades útiles en las clases diseñadas como singleton.                                                                    |
| [jf\Base\TToArray](src/TToArray.php)                                             | Serializa el contenido de la instancia iterando sobre todas las propiedades y analizando sus valores.                                                    |
