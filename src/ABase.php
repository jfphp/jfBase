<?php

namespace jf\Base;

use jf\Base\String\TStringable;
use Stringable;

/**
 * Clase que puede ser usada como base para algunos proyectos que implementa `Stringable`
 * y aporta un método `new` así algunos métodos para obtener el nombre de la clase en
 * diferentes formatos así como su espacio de nombres.
 */
abstract class ABase implements Stringable
{
    use TDasherize;
    use TStringable;

    /**
     * Construye la instancia de la clase especificada y le aplica los valores.
     *
     * @param array $values Valores a asignar a la instancia.
     *
     * @return static
     */
    public static function new(array $values = []) : static
    {
        return new static(...$values);
    }
}
