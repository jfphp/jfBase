<?php

namespace jf\Base;

/**
 * Interfaz para los singletons.
 */
interface ISingleton
{
    /**
     * Devuelve la instancia de la clase requerida.
     * Permite tener una sola instancia para clases heredadas.
     *
     * @return static
     */
    static public function i() : static;
}
