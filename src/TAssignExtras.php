<?php

namespace jf\Base;

/**
 * Permite asignar propiedades a una instancia a partir de un
 * array y almacenar aquellas que no pudieron asignarse.
 *
 * @mixin IAssign
 */
trait TAssignExtras
{
    use TAssign
    {
        TAssign::assign as private _assign;
    }
    use TToArray
    {
        TToArray::toArray as private _toArray;
    }

    /**
     * Propieades adicionales que intentaron asignarse pero
     * que no existen en la clase.
     *
     * @var array
     */
    public array $extras = [];

    /**
     * @see IAssign::assign()
     */
    public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array
    {
        $extras = $this->_assign($values, $map, $prefixes);
        foreach ($extras as $key => $value)
        {
            $this->extras[ $key ] = $value;
        }

        return $extras;
    }

    /**
     * @see IToArray::toArray()
     */
    public function toArray() : array
    {
        $data = $this->_toArray();
        if (array_key_exists('extras', $data))
        {
            $data = [ ...$data['extras'], ...$data ];
            unset($data['extras']);
        }

        return $data;
    }
}
