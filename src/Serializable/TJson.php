<?php

namespace jf\Base\Serializable;

/**
 * Trait para serializar datos en formato JSON.
 */
trait TJson
{
    /**
     * Indica si la deserialización devuelve un array asociativo o un objeto.
     *
     * @var bool
     */
    public bool $jsonAssociative = TRUE;

    /**
     * Opciones a pasar a la función `json_decode`.
     *
     * @var int
     */
    public int $jsonFlags = 0;

    /**
     * Opciones a usar al serializar los datos en formato JSON.
     *
     * @var int
     */
    public int $jsonOptions = JSON_PRESERVE_ZERO_FRACTION | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;

    /**
     * Deserializa los datos en formato JSON.
     *
     * @param string $data Datos a deserializar.
     *
     * @return mixed
     */
    public function fromJson(string $data) : mixed
    {
        return json_decode($data, $this->jsonAssociative, flags: $this->jsonFlags);
    }

    /**
     * Serializa los datos en formato JSON.
     *
     * @param mixed $data Datos a serializar.
     *
     * @return bool|string Texto JSON con los datos serializados o `FALSE` si ocurrió un error.
     */
    public function toJson(mixed $data) : bool|string
    {
        return json_encode($data, $this->jsonOptions);
    }
}
