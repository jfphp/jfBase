<?php

namespace jf\Base\Serializable;

use jf\Base\ABase;
use jf\Base\IAssign;
use jf\Base\IToArray;
use JsonSerializable;
use Serializable;

/**
 * Clase base para aquellos objetos que necesitan serialización.
 */
abstract class ASerializable extends ABase implements IAssign, IToArray, JsonSerializable, Serializable
{
    use TSerializable;
}
