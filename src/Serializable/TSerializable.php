<?php

namespace jf\Base\Serializable;

use jf\Base\IAssign;
use jf\Base\IToArray;
use JsonSerializable;
use Serializable;

/**
 * Trait que permite implementar de manera muy simple y rápida la serialización de objetos en arrays
 * así como inicializar el objeto desde un array.
 *
 * Solamente serializa los valores escalares, arrays y nulos. Su principal uso es para modelos de datos donde
 * se serializa para almacenar en base de datos.
 *
 * @mixin IAssign
 * @mixin IToArray
 * @mixin JsonSerializable
 * @mixin Serializable
 */
trait TSerializable
{
    use TAssign;
    use TToArray;

    /**
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.serialize
     */
    public function __serialize() : array
    {
        return $this->toArray();
    }

    /**
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.unserialize
     */
    public function __unserialize(array $data) : void
    {
        $this->assign($data);
    }

    /**
     * @see JsonSerializable::jsonSerialize()
     */
    public function jsonSerialize() : mixed
    {
        return $this->toArray();
    }

    /**
     * @see Serializable::serialize()
     */
    public function serialize() : ?string
    {
        return serialize($this);
    }

    /**
     * @see Serializable::unserialize()
     */
    public function unserialize(string $data) : void
    {
        unserialize($data);
    }
}
