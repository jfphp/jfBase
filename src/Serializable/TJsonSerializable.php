<?php

namespace jf\Base\Serializable;

use JsonSerializable;

/**
 * Trait que permite implementar la interfaz `JsonSerializable`.
 *
 * @mixin JsonSerializable
 */
trait TJsonSerializable
{
    use TToArray;

    /**
     * @see          JsonSerializable::jsonSerialize()
     *
     * @noinspection PhpMixedReturnTypeCanBeReducedInspection
     */
    public function jsonSerialize() : mixed
    {
        return $this->toArray();
    }
}
