<?php

namespace jf\Base\Serializable;

use jf\Base\IToArray;

/**
 * Serializa el contenido de la instancia iterando sobre todas las propiedades y devolviendo
 * los valores escalares, nulos y arrays.
 *
 * @mixin IToArray
 */
trait TToArray
{
    /**
     * @see ISerializable::toArray()
     */
    public function toArray() : array
    {
        $items = [];
        foreach ($this as $property => $value)
        {
            if ((is_int($property) || $property[0] !== '_') && ($value === NULL || is_scalar($value) || is_array($value)))
            {
                $items[ $property ] = $value;
            }
        }

        return $items;
    }
}
