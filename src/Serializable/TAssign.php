<?php

namespace jf\Base\Serializable;

use jf\Base\IAssign;
use jf\Base\TPascalize;

/**
 * Implementa la interfaz `jf\Base\IAssign` de manera simple asignando solamente las propiedades.
 *
 * @mixin IAssign
 */
trait TAssign
{
    use TPascalize;

    /**
     * @see IAssign::assign()
     */
    public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array
    {
        if ($values)
        {
            if ($map)
            {
                foreach ($map as $key => $prop)
                {
                    if (array_key_exists($key, $values))
                    {
                        $values[ $prop ] = $values[ $key ];
                        unset($values[ $key ]);
                    }
                }
            }
            foreach ($values as $property => $value)
            {
                if (property_exists($this, $property))
                {
                    $this->$property = $value;
                    unset($values[ $property ]);
                }
                else
                {
                    $pascalized = lcfirst(self::pascalize($property));
                    if ($property !== $pascalized && property_exists($this, $pascalized))
                    {
                        $this->$pascalized = $value;
                        unset($values[ $property ]);
                    }
                    elseif (($prop = "_$property") && property_exists($this, $prop))
                    {
                        $this->$prop = $value;
                        unset($values[ $property ]);
                    }
                }
            }
        }

        return $values;
    }

    /**
     * @see IAssign::fromArray()
     */
    public static function fromArray(array $values, array $map = []) : static
    {
        $instance = new static();
        $instance->assign($values, $map);

        return $instance;
    }
}
