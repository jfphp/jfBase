<?php

namespace jf\Base\Serializable;

/**
 * Trait para gestionar datos en formato YAML.
 */
trait TYaml
{
    /**
     * Tipo de fin de línea al generar el YAML.
     *
     * @var int
     */
    public int $yamlBreak = YAML_LN_BREAK;

    /**
     * Callbacks a ejecutar por el lector del YAML.
     *
     * @var array
     */
    public array $yamlCallbacks = [];

    /**
     * Codificación a usar para el YAML a generar.
     *
     * @var int
     */
    public int $yamlEncoding = YAML_UTF8_ENCODING;

    /**
     * Valor de la indentación a usar si se convierte a texto YAML el contenido.
     *
     * @var int
     */
    public int $yamlIndent = 4;

    /**
     * Ancho de línea.
     *
     * @var int
     */
    public int $yamlWidth = 132;

    /**
     * Deserializa los datos en formato YAML.
     *
     * @param string $data Datos a deserializar.
     *
     * @return array|NULL
     */
    public function fromYaml(string $data) : ?array
    {
        return yaml_parse($data, 0, $_, $this->yamlCallbacks);
    }

    /**
     * Serializa los datos en formato YAML.
     *
     * @param mixed $data Datos a serializar.
     *
     * @return string Texto YAML con los datos serializados o `FALSE` si ocurrió un error.
     */
    public function toYaml(mixed $data) : string
    {
        $indent = ini_get('yaml.output_indent');
        $width  = ini_get('yaml.output_width');
        //
        ini_set('yaml.output_indent', $this->yamlIndent);
        ini_set('yaml.output_width', $this->yamlWidth);
        //
        $string = yaml_emit($data, $this->yamlEncoding, $this->yamlBreak, $this->yamlCallbacks);
        //
        ini_set('yaml.output_indent', $indent);
        ini_set('yaml.output_width', $width);

        return $string;
    }
}
