<?php

namespace jf\Base;

/**
 * Interfaz para los elementos que requieren un identificador.
 */
interface IId
{
    /**
     * Devuelve el identificador del elemento.
     *
     * @return int|string|NULL
     */
    public function getId() : int|string|null;

    /**
     * Asigna el identificador del elemento.
     *
     * @param int|string|NULL $id Identificador a asignar.
     *
     * @return $this
     */
    public function setId(int|string|null $id) : static;
}