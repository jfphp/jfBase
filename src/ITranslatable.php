<?php

namespace jf\Base;

/**
 * Interfaz para marcar aquellos objetos que necesiten traducir los valores presentes en sus propiedades.
 *
 * Un ejemplo de serializador que detecte esta interfaz podría ser:
 *
 * ```pho
 * function serialize(object $obj) : array
 * {
 *     $translatables = $obj instanceof ITranslatable ? $obj::translatables() : [];
 *     $values        = [];
 *     foreach ($obj as $prop => $value)
 *     {
 *         $values[$prop] = array_key_exists($prop, $translatables)
 *             ? $obj->translatable($prop)
 *             : serializeValue($value);
 *     }
 *
 *     return $values;
 * }
 * ```
 */
interface ITranslatable
{
    /**
     * Devuelve en texto la representación del contenido de la clase.
     *
     * Lo más apropiado si no tiene una traducción es devolver el valor tal cual pero también
     * se puede decidir devolver `NULL` si la propiedad no tiene ningún valor asignado para
     * diferenciar cuando el valor de la propiedad es una cadena vacía y permitir usar un
     * valor por defecto con el operador `??`.
     *
     * @param string $property Nombre de la propiedad que se requiere devolver traducida.
     *
     * @return string|NULL
     *
     * @note Evitamos usar llamar al método `translate` para que no colisione al ser un nombre de método más común.
     */
    public function translatable(string $property) : ?string;

    /**
     * Devuelve el listado de nombres de propiedades de la clase que deben ser traducidas.
     *
     * @return string[]
     */
    public static function translatables() : array;
}