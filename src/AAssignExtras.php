<?php

namespace jf\Base;

/**
 * Clase que aporta la misma funcionalidad que `jf\Base\AAsign` permitiendo adicionalmente almacenar
 * aquellos valores que se trataron de asignar pero que no corresponden con propiedades de la clase.
 */
abstract class AAssignExtras extends AAssign
{
    use TAssignExtras;
}
