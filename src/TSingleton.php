<?php

namespace jf\Base;

/**
 * Contiene algunos métodos y propiedades útiles en las clases diseñadas como singleton.
 *
 * @mixin ISingleton
 */
trait TSingleton
{
    /**
     * Instancia generada de la clase.
     *
     * @var static|NULL
     */
    private static ?self $_instance = NULL;

    /**
     * Almacena las instancias generadas.
     * Permite utilizar la clase y sus derivadas como singleton.
     *
     * @var static[]
     */
    private static array $_instances = [];

    /**
     * Evitamos la creación del singleton desde fuera de la clase.
     */
    protected function __construct()
    {
    }

    /**
     * Evitamos la clonación del singleton.
     */
    private function __clone()
    {
    }

    /**
     * Evitamos la serialización del singleton.
     */
    public function __sleep()
    {
    }

    /**
     * Evitamos la deserialización del singleton.
     */
    public function __wakeUp()
    {
    }

    /**
     * Método usado para eliminar la instancia y poder realizar pruebas unitarias.
     *
     * @return void
     */
    public static function clearInstance() : void
    {
        static::$_instance = NULL;
    }

    /**
     * Método usado para eliminar todas las instancias y poder realizar pruebas unitarias.
     *
     * @return void
     */
    public static function clearInstances() : void
    {
        static::$_instances = [];
    }

    /**
     * Devuelve la instancia de la clase requerida.
     * Permite tener instancias diferentes para clases heredadas.
     *
     * @return static
     */
    public static function getInstance() : static
    {
        // Obtenemos el nombre completo de la clase para poder instanciarla.
        $class = static::class;
        if (!isset(static::$_instances[ $class ]))
        {
            static::$_instances[ $class ] = new $class();
        }

        return static::$_instances[ $class ];
    }

    /**
     * @see ISingleton::i())
     */
    public static function i() : static
    {
        if (!isset(static::$_instance))
        {
            static::$_instance = new static();
        }

        return static::$_instance;
    }
}
