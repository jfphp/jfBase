<?php

namespace jf\Base;

/**
 * Gestiona el acceso a una instancia padre.
 */
interface IParent
{
    /**
     * Devuelve la instancia padre.
     *
     * @return object
     */
    public function getParent() : object;

    /**
     * Devuelve la instancia ancestral del tipo especificada.
     * Si el nodo actual es de la clase buscada, se devuelve el nodo actual.
     *
     * @param string $classname Nombre de la clase a buscar.
     *
     * @return object
     */
    public function getParentOf(string $classname) : object;

    /**
     * Construye la instancia de la clase especificada y le aplica los valores.
     *
     * @param string $Class  Nombre de la clase de la instnacia a crear.
     * @param array  $values Valores a asignar a la instancia.
     *
     * @return ABase
     */
    public function withParent(string $Class = self::class, array $values = []) : ABase;
}
