<?php

namespace jf\Base;

/**
 * Permite implementar la interfaz `jf\Base\IId`.
 *
 * @mixin IId
 */
trait TId
{
    /**
     * Identificador del elemento.
     *
     * @var int|string|NULL
     */
    public int|string|null $id = NULL;

    /**
     * @see IId::getId()
     */
    public function getId() : int|string|null
    {
        return $this->id;
    }

    /**
     * @see IId::setId()
     */
    public function setId(int|string|null $id) : static
    {
        $this->id = $id;

        return $this;
    }
}