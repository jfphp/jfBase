<?php

namespace jf\Base;

/**
 * Aporta un método para convertir un texto a formato `PascalCase`.
 */
trait TPascalize
{
    /**
     * Convierte a `PascalCase` el texto.
     *
     * @param string $text Texto a convertir.
     *
     * @return string
     */
    public static function pascalize(string $text) : string
    {
        return preg_replace_callback(
            '/(?:^|[^[:alnum:]]+)([[:alnum:]])/u',
            fn($value) => mb_strtoupper($value[1], 'UTF-8'),
            $text
        ) ?: '';
    }
}
