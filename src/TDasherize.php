<?php

namespace jf\Base;

/**
 * Aporta métodos para obtener el nombre de la clase y/o su espacio
 * de nombre en formato `kebab-case`.
 */
trait TDasherize
{
    use TClass;

    /**
     * Convierte un texto en formato `CamelCase` usando un separador entre palabras.
     *
     * @param string $text      Texto a convertir.
     * @param string $separator Separador a usar.
     *
     * @return string
     */
    public static function dasherize(string $text, string $separator = '-') : string
    {
        return strtolower(preg_replace('/([^A-Z])([A-Z])/u', "\$1$separator\$2", $text));
    }

    /**
     * Devuelve el nombre de la clase de la instancia convirtiendo CamelCase a guiones.
     *
     * @return string
     */
    public function dasherized() : string
    {
        return static::dasherize($this->getClass(), '-');
    }

    /**
     * Devuelve el nombre de la clase convirtiendo CamelCase a guiones.
     *
     * @return string
     */
    public static function dasherizedCalledClass() : string
    {
        return static::dasherize(static::getCalledClass(), '-');
    }
}
