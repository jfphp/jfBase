<?php

namespace jf\Base;

/**
 * Interfaz para detectar el uso de los métodos `assign` y `fromArray`.
 */
interface IAssign
{
    /**
     * Asigna los valores de las propiedades de la instancia.
     *
     * @param array<string,mixed>  $values   Valores de las propiedades a asignar.
     * @param array<string,string> $map      Mapa para reasignar los nombres de las propiedades.
     * @param array                $prefixes Prefijo de los métodos a llamar en el orden especificado.
     *
     * @return array<string,mixed> Valores sin asignar.
     */
    public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array;

    /**
     * Crea una instancia e inicializa las propiedades a partir de los valores presentes en el array.
     *
     * @param array<string,mixed>  $values Valores a aplicar a la instancia creada.
     * @param array<string,string> $map    Mapa para relacionar las claves con las propiedades.
     *
     * @return static
     */
    public static function fromArray(array $values, array $map = []) : static;
}
