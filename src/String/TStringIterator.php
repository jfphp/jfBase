<?php

namespace jf\Base\String;

use Iterator;

/**
 * Trait que permite implementar la interfaz `Iterator` en una cadena de texto.
 *
 * @mixin Iterator<string>
 */
trait TStringIterator
{
    /**
     * Posición del iterador.
     *
     * @var int
     */
    private int $_iteratorPosition = -1;

    /**
     * Valor del texto.
     *
     * @var string
     */
    protected string $_string = '';

    /**
     * @see Iterator::current()
     */
    public function current() : ?string
    {
        return $this->_string[ $this->_iteratorPosition ] ?? NULL;
    }

    /**
     * @see Iterator::key()
     */
    public function key() : int
    {
        return $this->_iteratorPosition;
    }

    /**
     * @see Iterator::next()
     */
    public function next() : int
    {
        return (++$this->_iteratorPosition);
    }

    /**
     * @see Iterator::rewind()
     */
    public function rewind() : void
    {
        $this->_iteratorPosition = 0;
    }

    /**
     * @see Iterator::valid()
     */
    public function valid() : bool
    {
        return strlen($this->_string) > $this->_iteratorPosition;
    }
}
