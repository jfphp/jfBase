<?php

namespace jf\Base\String;

use jf\Base\AAssignExtras;

/**
 * Renderiza plantillas de textos sencillas.
 *
 * Ejemplo de uso rápido:
 *
 * ```php
 * echo Tpl::renderTpl('Hola {0}', [ 'Mundo' ])); // Hola Mundo
 * ```
 */
class Tpl extends AAssignExtras
{
    use TPlaceholders;

    /**
     * @inheritdoc
     */
    public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array
    {
        $this->addPlaceholders(parent::assign($values, $map, $prefixes));

        return [];
    }
}
