<?php

namespace jf\Base\String;

use InvalidArgumentException;

/**
 * Trait para gestionar valores de reemplazo y renderizar textos.
 */
trait TPlaceholders
{
    /**
     * Valores de reemplazo para renderizar los textos.
     *
     * @var array
     */
    protected array $_placeholders = [];

    /**
     * Caché para saber cuáles placeholders han sido usados y cuántas veces
     * y poder filtrar los que no son necesarios.
     *
     * @var array<string,string>
     */
    protected array $_usedPlaceholders = [];

    /**
     * Texto usado para cerrar un valor que debe ser reemplazado.
     *
     * @var string
     */
    public string $closePlaceholder = '}';

    /**
     * Listado de filtros disponibles.
     *
     * @var callable[]
     */
    protected array $_filters = [
        'base64'  => 'base64_encode',
        'b2h'     => 'bin2hex',
        'chr'     => 'chr',
        'crc32'   => 'crc32',
        'html'    => 'htmlentities',
        'lcfirst' => 'lcfirst',
        'len'     => 'strlen',
        'lower'   => 'strtolower',
        'ltrim'   => 'ltrim',
        'md5'     => 'md5',
        'nl2br'   => 'nl2br',
        'ord'     => 'ord',
        'qs'      => 'http_build_query',
        'rawurl'  => 'rawurlencode',
        'reverse' => 'strrev',
        'rtrim'   => 'rtrim',
        'sha'     => 'sha1',
        'trim'    => 'trim',
        'ucfirst' => 'ucfirst',
        'ucwords' => 'ucwords',
        'upper'   => 'strtoupper',
        'url'     => 'urlencode',
        'wrap'    => 'wordwrap'
    ];

    /**
     * Texto usado para abrir un valor que debe ser reemplazado.
     *
     * @var string
     */
    public string $openPlaceholder = '{';

    /**
     * Agrega un listado de filtros.
     *
     * @param array<string,callable> $filters Filtros a agregar.
     *
     * @return static
     */
    public function addFilters(array $filters) : static
    {
        foreach ($filters as $name => $callable)
        {
            if (is_callable($callable))
            {
                $this->_filters[ $name ] = $callable;
            }
        }

        return $this;
    }

    /**
     * Agrega los valores de reemplazo.
     *
     * @param array  $placeholders Valores de reemplazo a agregar.
     * @param string $prefix       Prefijo a usar para generar la clave.
     * @param bool   $escape       Indica si se escapan los valores para evitar errores al construir el documento.
     *                             Se deberían escapar aquellos valores que provienen del usuario.
     *                             Este método debe ser reimplementado con ese propósito por una clase que use el trait.
     *
     * @return static
     */
    public function addPlaceholders(array $placeholders, string $prefix = '', bool $escape = FALSE) : static
    {
        if ($prefix && $prefix[ -1 ] !== '.')
        {
            $prefix .= '.';
        }
        foreach ($placeholders as $key => $value)
        {
            if ($prefix)
            {
                $key = $prefix . $key;
            }
            if (is_array($value))
            {
                $this->addPlaceholders($value, $key, $escape);
            }
            else
            {
                $this->_placeholders[ $key ] = $value;
            }
        }

        return $this;
    }

    /**
     * Permite aplicar un filtro a un valor de reemplazo.
     *
     * @param string $filter Nombre del filtro a aplicar.
     * @param mixed  $value  Valor a modificar.
     *
     * @return string
     */
    protected function _filterPlaceholder(string $filter, mixed $value) : string
    {
        if (array_key_exists($filter, $this->_filters))
        {
            $value = $this->_filters[$filter]($value);
        }
        elseif (method_exists($this, $filter))
        {
            $value = $this->$filter($value);
        }

        return $value;
    }

    /**
     * Devuelve un valor de reemplazo.
     *
     * @param string $key Nombre del valor de reemplazo a devolver.
     *
     * @return scalar|NULL
     */
    public function getPlaceholder(string $key) : mixed
    {
        return $this->_placeholders[ $key ] ?? NULL;
    }

    /**
     * Devuelve los valores de reemplazo.
     *
     * @return array
     */
    public function getPlaceholders() : array
    {
        return $this->_placeholders;
    }

    /**
     * Devuelve los valores de reemplazo que han sido usados más de una vez.
     *
     * @return array<int|string,string>
     */
    public function getUsedPlaceholders() : array
    {
        return array_intersect_key($this->_placeholders, $this->_usedPlaceholders);
    }

    /**
     * Verifica si el valores de reemplazo está definido.
     *
     * @param string $key Nombre del valor de verificar.
     *
     * @return bool
     */
    public function hasPlaceholder(string $key) : bool
    {
        return array_key_exists($key, $this->_placeholders);
    }

    /**
     * Verifica si el valores de reemplazo ha sido usado.
     *
     * @param string $key Nombre del valor de verificar.
     *
     * @return bool
     */
    public function isPlaceholderUser(string $key) : bool
    {
        return array_key_exists($key, $this->_usedPlaceholders);
    }

    /**
     * Renderiza un texto usando los valores de reemplazo.
     *
     * @param string $tpl          Plantilla a renderizar.
     * @param array  $placeholders Valores de reemplazo.
     * @param bool   $throw        Indica si lanza una excepción  si no se encuentra un valor de reemplazo
     *                             para poder depurar más fácilmente los valores faltantes.
     * @param string $delimiter    Delimitador a usar en la expresión regular.
     *
     * @return string
     */
    public function render(string $tpl, array $placeholders = [], bool $throw = TRUE, string $delimiter = '/') : string
    {
        if ($tpl)
        {
            $open  = $this->openPlaceholder;
            $close = $this->closePlaceholder;
            $chars = preg_quote(implode('', array_unique([ ...str_split($open), ...str_split($close) ])), $delimiter);
            $tpl   = preg_replace_callback(
                $delimiter . preg_quote($open, $delimiter) . "([^$chars\n\r\t]+)" . preg_quote($close, $delimiter) . $delimiter,
                function ($matches) use ($open, $placeholders, $throw, $tpl) : string
                {
                    $parts = preg_split('/\s*\|\s*/', $matches[1]);
                    $key   = array_shift($parts);
                    $value = $placeholders[ $key ] ?? $this->_placeholders[ $key ] ?? NULL;
                    // Si el valor no tiene un reemplazo pero es un texto encerrado en comillas lo asumimos como un literal.
                    if ($value === NULL && strlen($key) > 2 && $key[0] === '"' && $key[ -1 ] === '"')
                    {
                        $value = substr($key, 1, -1);
                    }
                    if ($value !== NULL)
                    {
                        $this->_usedPlaceholders[ $key ] = $value;

                        $key = $value;
                        if ($key !== $tpl && is_string($key) && str_contains($key, $open))
                        {
                            $key = static::render($key, $placeholders, $throw);
                        }
                        foreach ($parts as $fn)
                        {
                            $key = $this->_filterPlaceholder($fn, $key);
                        }
                    }
                    elseif ($throw)
                    {
                        ksort($placeholders);
                        throw new InvalidArgumentException("Valor de reemplazo `$key` desconocido\n" . yaml_emit($placeholders));
                    }
                    else
                    {
                        $key = '';
                    }

                    return $key;
                },
                $tpl
            );
        }

        return $tpl;
    }

    /**
     * Método para renderizar un texto de manera usando los valores de reemplazos especificados.
     *
     * @param string $tpl          Plantilla a renderizar.
     * @param array  $placeholders Valores de reemplazo.
     * @param string $open         Texto de apertura de los valores de reemplazo.
     * @param string $close        Texto de cierre de los valores de reemplazo.
     *
     * @return string
     */
    public static function renderTpl(string $tpl, array $placeholders, string $open = '{', string $close = '}') : string
    {
        $obj                   = new static();
        $obj->closePlaceholder = $close;
        $obj->openPlaceholder  = $open;

        return $obj->render($tpl, $placeholders);
    }
}
