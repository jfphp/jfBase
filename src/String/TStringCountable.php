<?php

namespace jf\Base\String;

use Countable;

/**
 * Trait que permite implementar la interfaz `Countable` con una cadena de texto.
 *
 * @mixin Countable
 */
trait TStringCountable
{
    /**
     * Valor del texto.
     *
     * @var string
     */
    protected string $_string = '';

    /**
     * @see Countable::count()
     */
    public function count() : int
    {
        return strlen($this->_string);
    }
}
