<?php

namespace jf\Base\String;

use Stringable;

/**
 * Trait simple para implementar la interfaz `Stringable` devolviendo
 * como texto el nombre dela clase.
 *
 * @mixin Stringable
 */
trait TStringable
{
    /**
     * @see Stringable::__toString()
     */
    public function __toString() : string
    {
        return '[Class ' . static::class . ']';
    }
}
