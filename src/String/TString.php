<?php

namespace jf\Base\String;

/**
 * Trait para agrupar todos los traits que gestionan las cadenas de texto como arrays.
 */
trait TString
{
    use TStringAccess, TStringCountable, TStringIterator;

    /**
     * @inheritdoc
     */
    public function __construct(string $value = '')
    {
        $this->_string = $value;
    }

    /**
     * @inheritdoc
     */
    public function __serialize() : array
    {
        return [ 'string' => $this->_string ];
    }

    /**
     * @inheritdoc
     */
    public function __unserialize(array $data) : void
    {
        $this->_string = $data['string'] ?? '';
    }

    /**
     * Verifica si el resultado es una cadena de texto en cuyo caso la reasigna al
     * valor de la instancia.
     *
     * @param mixed $result Resultado a verificar.
     *
     * @return mixed Instancia actual si el resultado es una cadena de texto o `$result` en caso contrario.
     */
    protected function _checkResult(mixed $result) : mixed
    {
        if (is_string($result))
        {
            $this->_string = $result;
            $result        = $this;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize() : string
    {
        return $this->_string;
    }

    /**
     * @inheritdoc
     */
    public function serialize() : string
    {
        return serialize($this->_string);
    }

    /**
     * @inheritdoc
     */
    public function unserialize(string $serialized)
    {
        $this->_string = unserialize($serialized);
    }

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return $this->_string;
    }
}
