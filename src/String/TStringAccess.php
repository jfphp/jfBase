<?php

namespace jf\Base\String;

use ArrayAccess;
use jf\Base\TOffset;

/**
 * Trait que permite implementar la interfaz `ArrayAccess` con una cadena de texto.
 *
 * @mixin ArrayAccess<string>
 */
trait TStringAccess
{
    use TOffset;

    /**
     * Valor del texto.
     *
     * @var string
     */
    protected string $_string = '';

    /**
     * @see ArrayAccess::offsetExists()
     */
    public function offsetExists($offset) : bool
    {
        $len = strlen($this->_string);
        if (is_string($offset) && str_contains($offset, ':'))
        {
            $exists = static::_isOffsetValid($offset, $len) !== FALSE;
        }
        elseif (is_numeric($offset))
        {
            $exists = $offset >= 0
                ? $offset < $len
                : -$offset <= $len;
        }
        else
        {
            $exists = FALSE;
        }

        return $exists;
    }

    /**
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset) : ?string
    {
        if (is_string($offset) && str_contains($offset, ':'))
        {
            $range  = self::_isOffsetValid($offset, strlen($this->_string));
            $string = $range
                ? substr($this->_string, $range[0], $range[1] - $range[0] + 1)
                : NULL;
        }
        elseif (is_numeric($offset))
        {
            $string = $this->_string[ $offset ] ?? NULL;
        }
        else
        {
            $string = NULL;
        }

        return $string;
    }

    /**
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value) : void
    {
        $value = (string) $value;
        if (is_string($offset) && str_contains($offset, ':'))
        {
            $range = static::_isOffsetValid($offset, strlen($this->_string));
            if ($range)
            {
                $this->_string = substr($this->_string, 0, $range[0]) . $value . substr($this->_string, $range[1] + 1);
            }
        }
        elseif ($offset === NULL && $value)
        {
            $this->_string .= $value;
        }
        elseif (is_numeric($offset))
        {
            if ($offset < 0)
            {
                $offset = strlen($this->_string) + $offset;
            }
            $len = strlen($value);
            if ($len === 1)
            {
                if ($offset >= 0)
                {
                    $this->_string[ $offset ] = $value;
                }
            }
            else
            {
                $this->_string = substr($this->_string, 0, $offset) . $value . substr($this->_string, $offset + 1);
            }
        }
    }

    /**
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset) : void
    {
        $this->offsetSet($offset, '');
    }
}
