<?php

namespace jf\Base;

/**
 * Trait para calcular el offset en las clases que implementan `ArrayAccess` y aceptan el
 * formato `[from:to]` para acceder a los valores.
 */
trait TOffset
{
    /**
     * Calcula los índices del rango para el offset especificados.
     *
     * @param string $offset    Offset a convertir en rango.
     * @param int    $length    Longitud del valor al que se accederá con el offset.
     * @param string $separator Separador usado en el offset.
     *
     * @return int[]
     */
    private static function _calcOffset(string $offset, int $length, string $separator = ':') : array
    {
        [ $from, $to ] = explode($separator, $offset) + [ '', '' ];
        $from = $from === ''
            ? 0
            : intval($from);
        if ($from < 0)
        {
            $from = $length + $from;
        }
        $to = $to === ''
            ? $length - 1
            : intval($to);
        if ($to < 0)
        {
            $to = $length + $to;
        }
        elseif ($to >= $length)
        {
            $to = $length - 1;
        }

        return [ $from, $to ];
    }

    /**
     * Indica si el offset es válido para la longitud especificada.
     *
     * @param string $offset    Offset a validar.
     * @param int    $length    Longitud del valor al que se accederá con el offset.
     * @param string $separator Separador usado en el offset.
     *
     * @return array|bool
     */
    private static function _isOffsetValid(string $offset, int $length, string $separator = ':') : array|bool
    {
        [ $from, $to ] = static::_calcOffset($offset, $length, $separator);

        return $from <= $to && $from >= 0 && $from < $length && $to >= 0 && $to < $length
            ? [ $from, $to ]
            : FALSE;
    }
}
