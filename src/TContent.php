<?php

namespace jf\Base;

/**
 * Trait para gestionar el contenido de una instancia.
 */
trait TContent
{
    /**
     * Líneas a colocar al final del archivo.
     *
     * @var string[]
     */
    public array $footers = [];

    /**
     * Líneas a colocar al principio del archivo.
     *
     * @var string[]
     */
    public array $headers = [];

    /**
     * Contenido que será guardado.
     *
     * @var string[]
     */
    public array $content = [];

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return implode(PHP_EOL, array_filter([ ...$this->headers, ...array_map('strval', $this->content), ...$this->footers ]));
    }

    /**
     * Agrega los valores especificados al contenido actual.
     *
     * @param string|string[] $content Valores a agregar.
     *
     * @return static
     */
    public function addContent(array|string $content) : static
    {
        foreach ((is_array($content) ? $content : explode(PHP_EOL, $content)) as $line)
        {
            $this->content[] = $line;
        }

        return $this;
    }

    /**
     * Agrega una o más líneas al final del contenido.
     *
     * @param array|string $footers Líneas a agregar.
     *
     * @return static
     *
     * @assign
     */
    public function addFooters(array|string $footers) : static
    {
        foreach ((is_array($footers) ? $footers : explode(PHP_EOL, $footers)) as $line)
        {
            $this->headers[] = $line;
        }

        return $this;
    }

    /**
     * Agrega una o más líneas al principio del contenido.
     *
     * @param array|string $headers Líneas a agregar.
     *
     * @return static
     *
     * @assign
     */
    public function addHeaders(array|string $headers) : static
    {
        foreach ((is_array($headers) ? $headers : explode(PHP_EOL, $headers)) as $line)
        {
            $this->headers[] = $line;
        }

        return $this;
    }

    /**
     * Elimina el contenido actual.
     *
     * @return static
     */
    public function clearContent() : static
    {
        $this->content = [];

        return $this;
    }

    /**
     * Asigna el contenido de la instancia.
     *
     * @param string|string[] $content Valor a asignar.
     *
     * @return static
     */
    public function setContent(array|string $content) : static
    {
        $this->content = is_array($content)
            ? $content
            : explode(PHP_EOL, $content);

        return $this;
    }
}
