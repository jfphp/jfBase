<?php

namespace jf\Base;

use jf\Base\Serializable\ASerializable;

/**
 * Clase que aporta la misma funcionalidad que `jf\Base\ASerializable` pero con un método `toArray()`
 * más potente y que además implementa `jf\Base\IAssign` para facilitar la asignación de propiedades.
 */
abstract class AAssign extends ASerializable implements IAssign
{
    use TAssign;
    use TToArray;

    /**
     * Constructor de la clase.
     *
     * @param array $values Valores a usar para inicializar las propiedades de la clase.
     */
    public function __construct(array $values = [])
    {
        $this->__unserialize($values);
    }

    /**
     * @inheritdoc
     */
    public function __unserialize(array $data) : void
    {
        if ($data)
        {
            $this->assign($data);
        }
    }
}
