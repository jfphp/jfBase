<?php

namespace jf\Base;

/**
 * Interfaz para detectar el uso del método `toArray` y poder serializar un objeto.
 */
interface IToArray
{
    /**
     * Devuelve el contenido de la instancia en un array.
     *
     * @return array
     */
    public function toArray() : array;
}
