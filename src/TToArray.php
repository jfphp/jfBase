<?php

namespace jf\Base;

use BackedEnum;
use DateTimeInterface;
use JsonSerializable;
use UnitEnum;

/**
 * Serializa el contenido de la instancia iterando sobre todas las propiedades y analizando sus valores.
 *
 * @mixin IToArray
 */
trait TToArray
{
    /**
     * Construye el array con los valores que serán serializados.
     *
     * @return array
     */
    protected function buildToArrayValues() : array
    {
        $values = [];
        foreach ($this as $prop => $value)
        {
            if (is_int($prop) || $prop[0] !== '_')
            {
                $values[ $prop ] = $value;
            }
        }

        return $values;
    }

    /**
     * Serializa los valores presentes en un elemento iterable.
     *
     * @param array|object $values Valores a serializar.
     * @param bool         $empty
     *
     * @return array<string,scalar|scalar>
     */
    public static function serializeIterable(array|object $values, bool $empty = TRUE) : array
    {
        $data = [];
        foreach ($values as $key => $value)
        {
            if ($value)
            {
                if ($value instanceof IToArray)
                {
                    $value = $value->toArray();
                }
                elseif ($value instanceof JsonSerializable)
                {
                    $value = $value->jsonSerialize();
                }
                elseif ($value instanceof BackedEnum)
                {
                    $value = $value->value;
                }
                elseif ($value instanceof UnitEnum)
                {
                    $value = $value->name;
                }
                elseif ($value instanceof DateTimeInterface)
                {
                    $value = $value->format('c');
                }
                elseif (is_array($value) || is_object($value))
                {
                    $value = static::serializeIterable($value, $empty);
                }
            }
            if ($empty || ($value !== NULL && $value !== '' && $value !== []))
            {
                $data[ $key ] = $value;
            }
        }

        return $data;
    }

    /**
     * @see ISerializable::toArray()
     */
    public function toArray() : array
    {
        return static::serializeIterable($this->buildToArrayValues());
    }
}
