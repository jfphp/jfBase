<?php

namespace jf\Base\File;

use jf\Base\ABase;
use jf\Base\Serializable\TJsonSerializable;
use JsonSerializable;

/**
 * Clase que sirve de base para aquellos objetos que hacen uso intenso de archivos JSON.
 */
class Json extends ABase implements JsonSerializable
{
    use TJson;
    use TJsonSerializable;
}
