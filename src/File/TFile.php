<?php

namespace jf\Base\File;

use jf\Base\TContent;

/**
 * Agrega funcionalidad básica a clases que requieren leer/escribir archivos.
 */
trait TFile
{
    use TContent;
    use TFilename;
    use TPathInfo;
    use TReadWrite;

    /**
     * Carga el contenido del archivo.
     *
     * @return static
     */
    public function load() : static
    {
        $this->content = [ $this->read() ];

        return $this;
    }

    /**
     * Escribe en disco el contenido de la instancia.
     *
     * @return string
     */
    public function save() : string
    {
        $content = (string) $this;
        $this->write($content);

        return $content;
    }
}
