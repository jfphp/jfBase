<?php

namespace jf\Base\File;

/**
 * Trait para manipular el nombre de un archivo.
 */
trait TFilename
{
    /**
     * Ruta del archivo siendo gestionado por la instancia.
     *
     * @var string
     */
    public string $filename = '';

    /**
     * Calcula la ruta relativa entre 2 archivos.
     *
     * @param string $from Ruta inicial.
     * @param string $to   Ruta destino.
     *
     * @return string
     */
    public static function relativePath(string $from, string $to) : string
    {
        $offset = is_dir($from) || is_dir($to) ? 0 : 1;
        $from   = static::splitPath($from);
        $to     = static::splitPath($to);
        while ($from && $to && $from[0] === $to[0])
        {
            array_shift($from);
            array_shift($to);
        }

        return implode(DIRECTORY_SEPARATOR, [ ...array_fill(0, max(0, count($from) - $offset), '..'), ...$to ]);
    }

    /**
     * Asigna la ruta del archivo.
     *
     * @param string $filename Valor a asignar.
     *
     * @return static
     *
     * @assign
     */
    public function setFilename(string $filename) : static
    {
        $this->filename = is_file($filename)
            ? realpath($filename)
            : $filename;

        return $this;
    }

    /**
     * Separa la ruta de un archivo en sus segmentos omitiendo aquellos segmentos que están vacíos.
     *
     * @param string $path Ruta del archivo a separar.
     *
     * @return string[]
     */
    public static function splitPath(string $path) : array
    {
        return preg_split('|[\\\\/]|', $path, -1, PREG_SPLIT_NO_EMPTY);
    }
}
