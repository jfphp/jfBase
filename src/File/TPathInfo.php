<?php

namespace jf\Base\File;

/**
 * Trait para encapsular las llamadas a `pathinfo()`.
 */
trait TPathInfo
{
    /**
     * @see TFilename::$filename
     */
    public string $filename = '';

    /**
     * Devuelve el nombre del archivo y su extensión sin incluir el directorio.
     *
     * @return string
     */
    public function basename() : string
    {
        return pathinfo($this->filename, PATHINFO_BASENAME);
    }

    /**
     * Devuelve el nombre del directorio donde se encuentra el archivo.
     *
     * @return string
     */
    public function dirname() : string
    {
        return pathinfo($this->filename, PATHINFO_DIRNAME);
    }

    /**
     * Devuelve la extensión del nombre del archivo.
     *
     * @return string
     */
    public function extension() : string
    {
        return pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    /**
     * Devuelve el nombre del archivo sin la extensión.
     *
     * @return string
     */
    public function filename() : string
    {
        return pathinfo($this->filename, PATHINFO_FILENAME);
    }

    /**
     * Devuelve los elementos que componen la ruta del archivo.
     *
     * @return string[]
     */
    public function pathinfo() : array
    {
        return pathinfo($this->filename);
    }
}
