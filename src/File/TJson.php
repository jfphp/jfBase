<?php

namespace jf\Base\File;

use jf\Base\Serializable\TJson as TSerializableJson;

/**
 * Trait para gestionar archivos JSON.
 */
trait TJson
{
    use TSerializableJson;

    /**
     * Carga un archivo JSON y lo devuelve deserializado.
     *
     * @param string     $filename Ruta del archivo JSON a cargar.
     * @param array|NULL $defvalue Valor a devolver por defecto cuando el archivo no
     *                             exista o no se pueda deserializar.
     *
     * @return array|NULL
     */
    public function loadJson(string $filename, ?array $defvalue = []) : ?array
    {
        return is_file($filename)
            ? $this->fromJson(file_get_contents($filename)) ?? $defvalue
            : $defvalue;
    }

    /**
     * Guarda los datos en un archivo en formato JSON.
     *
     * @param string       $filename Ruta del archivo donde se guardarán los datos.
     * @param array|string $data     Datos a guardar.
     *
     * @return bool|int Cantidad de bytes escritos o `FALSE` si ocurrió un problema.
     */
    public function saveJson(string $filename, array|string $data) : bool|int
    {
        if (!is_string($data))
        {
            $data = $this->toJson($data);
        }

        return file_put_contents($filename, $data);
    }
}
