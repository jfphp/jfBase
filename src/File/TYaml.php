<?php

namespace jf\Base\File;

use jf\Base\Serializable\TYaml as TSerializableYaml;

/**
 * Trait para gestionar archivos YAML.
 */
trait TYaml
{
    use TSerializableYaml;

    /**
     * Carga un archivo YAML y lo devuelve deserializado.
     *
     * @param string     $filename Ruta del archivo YAML a cargar.
     * @param array|NULL $defvalue Valor a devolver por defecto cuando el archivo no
     *                             exista o no se pueda deserializar.
     *
     * @return array|NULL
     */
    public function loadYaml(string $filename, ?array $defvalue = []) : ?array
    {
        return is_file($filename)
            ? $this->fromYaml(file_get_contents($filename)) ?: $defvalue
            : $defvalue;
    }

    /**
     * Guarda los datos en un archivo en formato YAML.
     *
     * @param string       $filename Ruta del archivo donde se guardarán los datos.
     * @param array|string $data     Datos a guardar.
     *
     * @return bool|int Cantidad de bytes escritos o `FALSE` si ocurrió un problema.
     */
    public function saveYaml(string $filename, array|string $data) : bool|int
    {
        return file_put_contents($filename, $this->toYaml($data));
    }
}
