<?php

namespace jf\Base\File;

use jf\Base\ABase;
use jf\Base\TAssign;

/**
 * Clase que sirve de base para aquellos objetos que hacen uso intenso de archivos YAML.
 */
class Yaml extends ABase
{
    use TAssign;
    use TYaml;
}
