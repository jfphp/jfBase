<?php

namespace jf\Base\File;

use jf\Base\Exception;
use Psr\Log\LoggerInterface;

/**
 * Trait para gestionar la lectura y escritura de archivos.
 */
trait TReadWrite
{
    /**
     * Máscara de bits a usar para asignar los permisos del archivo cuando es creado/sobrescrito.
     *
     * @var int|NULL
     */
    public ?int $chmod = 0o600;

    /**
     * Activa el modo depuración donde el contenido se muestra por pantalla en vez de escribirse en disco.
     *
     * @var bool
     */
    public bool $debug = TRUE;

    /**
     * @see TFilename::$filename
     */
    public string $filename = '';

    /**
     * Logger a usar para informar de las acciones realizadas.
     *
     * @var LoggerInterface|NULL
     */
    public ?LoggerInterface $logger = NULL;

    /**
     * Indica si se sobrescriben los archivos existentes.
     *
     * @var bool
     */
    public bool $overwrite = FALSE;

    /**
     * Carga el contenido del archivo.
     *
     * @return string|NULL
     */
    public function read() : ?string
    {
        $filename = $this->filename;
        if ($filename && is_file($filename))
        {
            $logger  = $this->logger;
            $content = Exception::read($filename);
            $logger?->debug(dgettext('base', "Archivo {0} leído: {1} bytes\n"), [ $filename, strlen($content) ]);
        }
        else
        {
            $content = NULL;
        }

        return $content;
    }

    /**
     * Encierra en un recuadro el texto especificado.
     *
     * @param string $text Texto a formatear.
     *
     * @return string
     */
    public static function rect(string $text) : string
    {
        $_sep = str_repeat('─', mb_strlen($text) + 2);

        return sprintf("┌%s┐\n│ %s │\n└%s┘\n", $_sep, $text, $_sep);
    }

    /**
     * Guarda el contenido en un archivo.
     *
     * @param string $content Contenido a guardar.
     *
     * @return string
     */
    public function write(string $content) : string
    {
        $filename = $this->filename ?: 'php://stdout';
        if ($this->debug)
        {
            echo static::rect($filename) . $content . PHP_EOL;
        }
        else
        {
            $logger = $this->logger;
            if (str_contains($filename, '://'))
            {
                $bytes = Exception::writeWrapper($filename, $content);
                $logger?->debug(dgettext('base', "Escritos {0} bytes en el archivo {1}\n"), [ $bytes, $filename ]);
            }
            elseif ($this->overwrite || !is_file($filename))
            {
                if (is_file($filename) && filesize($filename) === strlen($content) && file_get_contents($filename) === $content)
                {
                    $logger?->debug(dgettext('base', "Archivo {0} idéntico\n"), [ $filename ]);
                }
                else
                {
                    $chmod   = $this->chmod;
                    $umask   = $chmod === NULL ? NULL : umask(0);
                    $dirname = dirname($filename);
                    if (!Exception::isDir($dirname))
                    {
                        $logger?->debug(dgettext('base', "Creado el directorio {0}\n"), [ $dirname ]);
                    }
                    Exception::isWritable($dirname);
                    $bytes = Exception::writeFile($filename, $content);
                    $logger?->debug(dgettext('base', "Escritos {0} bytes en el archivo {1}\n"), [ $bytes, $filename ]);
                    if ($chmod !== NULL)
                    {
                        chmod($filename, $chmod);
                    }
                    umask($umask);
                }
            }
        }

        return $content;
    }
}
