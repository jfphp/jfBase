<?php

namespace jf\Base\File;

use jf\Base\ABase;
use jf\Base\IAssign;
use jf\Base\TAssign;

/**
 * Lee/escribe contenido desde/a un archivo.
 */
class File extends ABase implements IAssign
{
    use TAssign;
    use TFile;
}
