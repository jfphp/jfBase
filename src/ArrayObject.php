<?php

namespace jf\Base;

use ArrayObject as PhpArrayObject;

/**
 * Clase usada donde es requerido un objeto de tipo `\ArrayObject` pero que
 * implemente `jf\Base\IAssign` e `jf\Base\IToArray`.
 *
 * En algunos casos puede ser preferible extender de `jf\Base\Assign` y agregar los traits existentes
 * dentro del espacio de nombres `jf\Array` en vez de requerir un objeto de tipo `\ArrayObject`.
 */
class ArrayObject extends PhpArrayObject implements IAssign, IToArray
{
    use TAssign
    {
        TAssign::assign as private _assign;
    }

    /**
     * @inheritDoc
     */
    public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array
    {
        foreach ($this->_assign($values, $map, $prefixes) as $key => $value)
        {
            $this[ $key ] = $value;
        }

        return [];
    }

    /**
     * Devuelve un array fusionando los valores actuales con los especificados usando la
     * función `array_merge` de PHP.
     *
     * @param array $data Valos a fusionar los actuales.
     *
     * @return array
     */
    public function merge(array $data) : array
    {
        return $data
            ? array_merge($this->toArray(), $data)
            : $this->toArray();
    }

    /**
     * Devuelve un array fusionando los valores actuales con los especificados usando la
     * función `array_merge_recursive` de PHP.
     *
     * @param array $data Valores a fusionar con los actuales.
     *
     * @return array
     */
    public function mergeRecursive(array $data) : array
    {
        return $data
            ? array_merge_recursive($this->toArray(), $data)
            : $this->toArray();
    }

    /**
     * Devuelve un array fusionando los valores actuales con los especificados usando la
     * función `array_replace` de PHP.
     *
     * @param array $data Valores a fusionar con los actuales.
     *
     * @return array
     */
    public function replace(array $data) : array
    {
        return $data
            ? array_replace($this->toArray(), $data)
            : $this->toArray();
    }

    /**
     * Devuelve un array fusionando los valores actuales con los especificados usando la
     * función `array_replace_recursive` de PHP.
     *
     * @param array $data Valores a fusionar con los actuales.
     *
     * @return array
     */
    public function replaceRecursive(array $data) : array
    {
        return $data
            ? array_replace_recursive($this->toArray(), $data)
            : $this->toArray();
    }

    /**
     * @inheritdoc
     */
    public function toArray() : array
    {
        return $this->getArrayCopy();
    }
}
