<?php

namespace jf\Base;

/**
 * Interfaz para detectar aquellas clases que se pueden convertir a `string` pero que
 * no aceptan la interfaz `Stringable`.
 *
 * Un ejemplo de estas clases son las enumeraciones.
 */
interface IStringable
{
    /**
     * Devuelve en texto la representación del contenido de la clase.
     *
     * @return string
     */
    public function toString() : string;
}