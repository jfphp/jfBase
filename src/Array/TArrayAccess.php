<?php

namespace jf\Base\Array;

use ArrayAccess;
use jf\Base\TOffset;

/**
 * Trait que permite implementar la interfaz `ArrayAccess`.
 *
 * @template T
 *
 * @mixin ArrayAccess<T>
 */
trait TArrayAccess
{
    use TOffset;

    /**
     * Listado de elementos almacenados en la instancia.
     *
     * @var array
     */
    protected array $_items = [];

    /**
     * @see ArrayAccess::offsetExists()
     */
    public function offsetExists($offset) : bool
    {
        return isset($this->_items[ $offset ]);
    }

    /**
     * @return T|T[]|NULL
     *
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset) : mixed
    {
        $items = $this->_items;
        if (is_numeric($offset))
        {
            $item = $items[ $offset ] ?? NULL;
        }
        elseif (is_string($offset))
        {
            if (str_contains($offset, ':'))
            {
                $range = self::_isOffsetValid($offset, count($items));
                $item  = $range
                    ? array_slice($items, $range[0], $range[1] - $range[0] + 1, TRUE)
                    : NULL;
            }
            else
            {
                $item = $items[ $offset ] ?? NULL;
            }
        }
        else
        {
            $item = NULL;
        }

        return $item;
    }

    /**
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value) : void
    {
        if ($offset === NULL)
        {
            $this->_items[] = $value;
        }
        else
        {
            $this->_items[ $offset ] = $value;
        }
    }

    /**
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset) : void
    {
        unset($this->_items[ $offset ]);
    }
}
