<?php

namespace jf\Base\Array;

use Countable;

/**
 * Trait que permite implementar la interfaz `Countable`.
 *
 * @template T
 *
 * @mixin Countable
 */
trait TArrayCountable
{
    /**
     * Listado de elementos almacenados en la instancia.
     *
     * @var array<T>
     */
    protected array $_items = [];

    /**
     * @see Countable::count()
     */
    public function count() : int
    {
        return count($this->_items);
    }
}
