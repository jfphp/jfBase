<?php

namespace jf\Base\Array;

use Iterator;

/**
 * Trait que permite implementar la interfaz `Iterator`.
 *
 * @template T
 *
 * @mixin Iterator<T>
 */
trait TArrayIterator
{
    /**
     * Listado de elementos almacenados en la instancia.
     *
     * @var array<T>
     */
    protected array $_items = [];

    /**
     * @return T|NULL
     *
     * @see Iterator::current()
     */
    public function current() : mixed
    {
        return current($this->_items);
    }

    /**
     * @see Iterator::key()
     */
    public function key() : int|string|null
    {
        return key($this->_items);
    }

    /**
     * @see Iterator::next()
     */
    public function next() : void
    {
        next($this->_items);
    }

    /**
     * @see Iterator::rewind()
     */
    public function rewind() : void
    {
        reset($this->_items);
    }

    /**
     * @see Iterator::valid()
     */
    public function valid() : bool
    {
        return key($this->_items) !== NULL;
    }
}
