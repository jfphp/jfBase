<?php

namespace jf\Base\Array;

use ArrayAccess;
use Countable;
use Iterator;
use IteratorAggregate;

/**
 * Trait que permite implementar las interfaces `ArrayAccess`, `Countable`, `Iterator`
 * e `IteratorAggregate` en una clase que se quiera usar como un array.
 *
 * @template T
 *
 * @mixin ArrayAccess<T>
 * @mixin Countable<T>
 * @mixin Iterator<T>
 * @mixin IteratorAggregate<T>
 */
trait TArray
{
    /**
     * @use TArrayAccess<T>
     */
    use TArrayAccess;

    /**
     * @use TArrayCountable<T>
     */
    use TArrayCountable;

    /**
     * @use TArrayIterator<T>
     */
    use  TArrayIterator;

    /**
     * @use TArrayIteratorAggregate<T>
     */
    use  TArrayIteratorAggregate;

    /**
     * Elimina todos los elementos de la colección.
     *
     * @return static
     */
    public function clear() : static
    {
        $this->_items = [];

        return $this;
    }

    /**
     * Verifica si la instancia contiene algún elemento que contenga el valor especificado.
     *
     * @param scalar $value Valor a buscar.
     *
     * @return bool
     */
    public function contains(bool|float|int|string|null $value) : bool
    {
        return $this->some(fn($item) => $item === $value);
    }

    /**
     * Aplica la función a todos los elementos almacenados en la instancia.
     *
     * @param callable $fn Función a ejecutar para cada uno de los elementos.
     *                     Recibe como primer parámetro el valor y como segundo la clave.
     *
     * @return static
     */
    public function each(callable $fn) : static
    {
        foreach ($this as $key => $value)
        {
            $fn($value, $key);
        }

        return $this;
    }

    /**
     * Devuelve el último elemento del listado.
     *
     * @return T
     */
    public function end() : mixed
    {
        return end($this->_items);
    }

    /**
     * Verifica si la función es verdadera para todos los elementos de la instancia.
     *
     * @param callable $fn Función a ejecutar para cada uno de los elementos.
     *                     Recibe como primer parámetro el valor y como segundo la clave.
     *
     * @return bool
     */
    public function every(callable $fn) : bool
    {
        $every = TRUE;
        foreach ($this as $key => $value)
        {
            if (!$fn($value, $key))
            {
                $every = FALSE;
                break;
            }
        }

        return $every;
    }

    /**
     * Devuelve el primer elemento sin mover el puntero del listado.
     *
     * @return T|NULL
     */
    public function first() : mixed
    {
        return $this[ array_key_first($this->_items) ];
    }

    /**
     * Crea una instancia a partir del listado de elementos.
     *
     * @param T[] $items Listado de elementos gestionados por la instancia.
     *
     * @return static
     */
    public static function fromItems(array $items) : static
    {
        $instance = new static();
        foreach ($items as $item)
        {
            $instance->push($item);
        }

        return $instance;
    }

    /**
     * Devuelve el elemento especificado por su clave o índice.
     *
     * @param int|string $key Clave o índice del elemento a devolver.
     *
     * @return T
     */
    public function getItem(int|string $key) : mixed
    {
        return $this->_items[ $key ] ?? NULL;
    }

    /**
     * Devuelve el listado de elementos.
     *
     * @return T[]
     */
    public function getItems() : array
    {
        return $this->_items;
    }

    /**
     * Verifica si el elemento existe en la instanca.
     *
     * @param int|string $name Nombre o índice del elemento a verificar.
     *
     * @return bool
     */
    public function hasItem(int|string $name) : bool
    {
        return array_key_exists($name, $this->_items);
    }

    /**
     * Devuelve las claves del array.
     *
     * @return array<int|string>
     */
    public function keys() : array
    {
        return array_keys($this->_items);
    }

    /**
     * Devuelve el último elemento sin mover el puntero del listado.
     *
     * @return T|NULL
     */
    public function last() : mixed
    {
        return $this[ array_key_last($this->_items) ];
    }

    /**
     * Elimina el último elemento y lo devuelve.
     *
     * @return T|NULL
     */
    public function pop() : mixed
    {
        return array_pop($this->_items);
    }

    /**
     * Agrega un elemento al final de la lista.
     *
     * @param T $item Elemento a agregar.
     *
     * @return static
     */
    public function push(mixed $item) : static
    {
        $this->_items[] = $item;

        return $this;
    }

    /**
     * Elimina el elemento especificado.
     *
     * @param int|string $name Clave o índice del elemento a eliminar.
     *
     * @return static
     */
    public function removeItem(int|string $name) : static
    {
        unset($this->_items[ $name ]);

        return $this;
    }

    /**
     * Elimina el primer elemento y lo devuelve.
     *
     * @return T|NULL
     */
    public function shift() : mixed
    {
        return array_shift($this->_items);
    }

    /**
     * Verifica si la función es verdadera para al menos un elemento del listado.
     *
     * @param callable $fn Función a ejecutar para cada uno de los elementos.
     *                     Recibe como primer parámetro el valor y como segundo la clave.
     *
     * @return bool
     */
    public function some(callable $fn) : bool
    {
        $some = FALSE;
        foreach ($this as $key => $value)
        {
            if ($fn($value, $key))
            {
                $some = TRUE;
                break;
            }
        }

        return $some;
    }

    /**
     * Ordena las claves de la colección usando la función especificada.
     *
     * @param callable|int|NULL $sorter Función a usar para ordenar las claves.
     *
     * @return static
     */
    public function sortKeys(callable|int|null $sorter = NULL) : static
    {
        if (is_callable($sorter))
        {
            uksort($this->_items, $sorter);
        }
        else
        {
            ksort($this->_items, $sorter ?? SORT_REGULAR);
        }

        return $this;
    }

    /**
     * Ordena los valores de la colección usando la función especificada.
     *
     * @param callable|int|NULL $sorter Función a usar para ordenar los valores.
     *
     * @return static
     */
    public function sortValues(callable|int|null $sorter = NULL) : static
    {
        if (is_callable($sorter))
        {
            uasort($this->_items, $sorter);
        }
        else
        {
            asort($this->_items, $sorter ?? SORT_REGULAR);
        }

        return $this;
    }

    /**
     * Agrega un elemento al principio de la lista.
     *
     * @param T $item Elemento a agregar.
     *
     * @return static
     */
    public function unshift(mixed $item) : static
    {
        array_unshift($this->_items, $item);

        return $this;
    }

    /**
     * Retorna los valores del array sin incluir los índices.
     *
     * @return T[]
     */
    public function values() : array
    {
        return array_values($this->_items);
    }
}
