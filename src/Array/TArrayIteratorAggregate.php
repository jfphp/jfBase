<?php

namespace jf\Base\Array;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

/**
 * Trait que permite implementar la interfaz `IteratorAggregate`.
 *
 * @template T
 *
 * @mixin IteratorAggregate<T>
 */
trait TArrayIteratorAggregate
{
    /**
     * Listado de elementos almacenados en la instancia.
     *
     * @var array<T>
     */
    protected array $_items = [];

    /**
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator() : Traversable
    {
        return new ArrayIterator($this->_items);
    }
}
