<?php

namespace jf\Base;

/**
 * Aporta algunos métodos y propiedades útiles para obtener el nombre de la clase
 * y su espacio de nombres.
 */
trait TClass
{
    /**
     * Devuelve el nombre de la clase sin incluir el espacio de nombres.
     *
     * @return string
     */
    public static function getCalledClass() : string
    {
        $class = static::class;
        $index = strrpos($class, '\\');

        return $index === FALSE
            ? $class
            : substr($class, $index + 1);
    }

    /**
     * Devuelve el nombre de la clase de la instancia sin incluir el espacio de nombres.
     *
     * @return string
     */
    public function getClass() : string
    {
        return static::getCalledClass();
    }

    /**
     * Devuelve el espacio de nombre de la clase.
     *
     * @return string
     */
    public static function getCalledNamespace() : string
    {
        $index = strrpos(static::class, '\\');

        return $index === FALSE
            ? ''
            : substr(static::class, 0, $index);
    }

    /**
     * Devuelve el espacio de nombre de la clase de la instancia.
     *
     * @return string
     */
    public function getNamespace() : string
    {
        return static::getCalledNamespace();
    }
}
