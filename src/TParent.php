<?php

namespace jf\Base;

/**
 * Gestiona el acceso a una instancia padre.
 *
 * @mixin IParent
 */
trait TParent
{
    /**
     * Instancia que ha generado la actual.
     *
     * @var object|NULL
     */
    protected ?object $_parent = NULL;

    /**
     * @see IParent::getParent()
     */
    public function getParent() : ?object
    {
        return $this->_parent;
    }

    /**
     * @see IParent::getParentOf()
     */
    public function getParentOf(string $classname) : ?object
    {
        $parent = $this;
        while ($parent)
        {
            if (is_a($parent, $classname))
            {
                break;
            }
            elseif ($parent instanceof IParent)
            {
                $parent = $parent->getParent();
            }
            else
            {
                $parent = NULL;
                break;
            }
        }

        return $parent;
    }

    /**
     * @see IParent::withParent()
     */
    public function withParent(string $Class = self::class, array $values = []) : ABase
    {
        $values['parent'] = $this;

        return new $Class($values);
    }
}
