<?php

namespace jf\Base;

use Exception as PhpException;
use jf\Base\String\Tpl;

/**
 * Excepciones del paquete `jf/base`.
 */
class Exception extends PhpException
{
    /**
     * Error cuando el directorio no existe o no puede ser creado.
     */
    public const int ERROR_NO_DIR = 100;

    /**
     * Error al leer en el dispositivo o wrapper.
     */
    public const int ERROR_READ = 101;

    /**
     * Error al escribir en el dispositivo o wrapper.
     */
    public const int ERROR_WRITE = 102;

    /**
     * Error cuando no se puede escribir en el directorio.
     */
    public const int ERROR_UNWRITABLE = 103;

    /**
     * Verifica que el directorio exista o pueda ser creado.
     *
     * @param string $directory Ruta del directorio a verificar.
     *
     * @return bool `TRUE` si el directorio ya existía y `FALSE` si tuvo que ser creado.
     *
     * @noinspection PhpUsageOfSilenceOperatorInspection
     */
    public static function isDir(string $directory) : bool
    {
        if (is_dir($directory))
        {
            $return = TRUE;
        }
        elseif (@mkdir($directory, 0o777, TRUE))
        {
            $return = FALSE;
        }
        else
        {
            throw new static(
                Tpl::renderTpl(dgettext('base', 'No se pudo crear el directorio {0}'), [ $directory ]),
                self::ERROR_NO_DIR
            );
        }

        return $return;
    }

    /**
     * Verifica que se pueda escribir en la ruta especificada.
     *
     * @param string $pathname Ruta a verificar.
     *
     * @return void
     */
    public static function isWritable(string $pathname) : void
    {
        if (!is_writable($pathname))
        {
            throw new static(
                Tpl::renderTpl(dgettext('base', 'No se pudo escribir en el directorio {0}'), [ $pathname ]),
                self::ERROR_UNWRITABLE
            );
        }
    }

    /**
     * Verifica que se pueda leer desde el wrapper o dispositivo.
     *
     * @param string $filename Ruta del archivo o wrapper a leer.
     *
     * @return string
     */
    public static function read(string $filename) : string
    {
        $content = file_get_contents($filename);
        if ($content === FALSE)
        {
            throw new static(Tpl::renderTpl(dgettext('base', 'No se pudo leer {0}'), [ $filename ]), self::ERROR_READ);
        }

        return $content;
    }

    /**
     * Verifica que se haya podido escribir en el archivo.
     *
     * @param string $filename Ruta del archivo a escribir.
     * @param string $content  Contenido a escribir en el archivo.
     *
     * @return int Cantidad de bytes escritos.
     */
    public static function writeFile(string $filename, string $content) : int
    {
        $bytes = file_put_contents($filename, $content);
        if ($bytes === FALSE)
        {
            throw new static(
                Tpl::renderTpl(dgettext('base', 'No se pudo escribir en el archivo {0}'), [ $filename ]),
                self::ERROR_WRITE
            );
        }

        return $bytes;
    }

    /**
     * Verifica que se haya podido escribir en el wrapper.
     *
     * @param string $filename Ruta del wrapper a escribir.
     * @param string $content  Contenido a escribir en el wrapper.
     *
     * @return int Cantidad de bytes escritos.
     */
    public static function writeWrapper(string $filename, string $content) : int
    {
        $bytes = file_put_contents($filename, $content);
        if ($bytes === FALSE)
        {
            throw new static(
                Tpl::renderTpl(dgettext('base', 'No se pudo escribir en el wrapper {0}'), [ $filename ]),
                self::ERROR_WRITE
            );
        }

        return $bytes;
    }
}
