<?php

namespace jf\Base;

/**
 * Permite asignar propiedades a una instancia a partir de un array.
 *
 * @mixin IAssign
 */
trait TAssign
{
    use TPascalize;

    /**
     * @see IAssign::assign()
     */
    public function assign(array $values, array $map = [], array $prefixes = [ 'set', 'add' ]) : array
    {
        $unused = [];
        if ($map)
        {
            foreach ($map as $key => $prop)
            {
                if (array_key_exists($key, $values))
                {
                    $values[ $prop ] = $values[ $key ];
                    unset($values[ $key ]);
                }
            }
        }
        // El orden es importante así que creamos un listado vacío para que luego se itere en ese mismo orden.
        $hooks = array_combine($prefixes, array_fill(0, count($prefixes), []));
        foreach ($values as $name => $value)
        {
            $suffix = static::pascalize($name);
            $found  = FALSE;
            foreach ($prefixes as $prefix)
            {
                $hook = $prefix . $suffix;
                if (method_exists($this, $hook))
                {
                    $hooks[ $prefix ][ $hook ] = $value;
                    $found                     = TRUE;
                    break;
                }
            }
            if (!$found)
            {
                if (property_exists($this, $name))
                {
                    $this->$name = $value;
                }
                elseif (($prop = "_$name") && property_exists($this, $prop))
                {
                    $this->$prop = $value;
                }
                else
                {
                    $unused[ $name ] = $value;
                }
            }
        }
        // Ahora vamos con los `setters`, que tienen prioridad sobre los `adders`.
        foreach ($hooks as $methods)
        {
            foreach ($methods as $method => $value)
            {
                $this->$method($value);
            }
        }

        return $unused;
    }

    /**
     * @see IAssign::fromArray()
     */
    public static function fromArray(array $values, array $map = []) : static
    {
        $instance = new static();
        $instance->assign($values, $map);

        return $instance;
    }
}
