��    
      l      �       �      �           '  %   A  "   g  %   �  (   �  %   �     �  )       =     Q     n     �     �     �      �     �          
   	                                     Archivo {0} idéntico
 Archivo {0} leído: {1} bytes
 Creado el directorio {0}
 Escritos {0} bytes en el archivo {1}
 No se pudo crear el directorio {0} No se pudo escribir en el archivo {0} No se pudo escribir en el directorio {0} No se pudo escribir en el wrapper {0} No se pudo leer {0} Project-Id-Version: jf/base 4.1.0
Report-Msgid-Bugs-To: Joaquín Fernández
PO-Revision-Date: 2024-09-28 22:54:38+0200
Last-Translator: Joaquín Fernández
Language-Team: Joaquín Fernández
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 File {0} identical
 Reading file {0}: {1} bytes
 Directorio {0} created
 Writing file {1}: {0} bytes
 Could not create directory {0} Could not write to file {0} Could not write to directory {0} Could not write to wrapper {0} Could not read from file {0} 